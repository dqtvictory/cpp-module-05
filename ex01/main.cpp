#include "Bureaucrat.hpp"
#include "Form.hpp"

int	main(void)
{
	Bureaucrat	buro("Buro", 10);
	Form		f1("Important doc", 5, 3);
	Form		f2("Whatever doc", 100, 90);
	Form		f3 = f2;
	Form		f4(f3);
	Form		*f5 = new Form("Normal doc", 10, 10);

	std::cout << buro << f1 << f2 << f3 << f4 << *f5 << '\n';
	
	buro.signForm(f1);
	buro.signForm(f2);
	buro.signForm(f3);
	buro.signForm(f4);
	buro.signForm(*f5);

	std::cout << '\n' << f1 << f2 << f3 << f4 << *f5;

	delete f5;
}