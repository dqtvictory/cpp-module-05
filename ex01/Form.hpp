#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"

class Form
{

private:
	const std::string	_name;
	const unsigned int	_gradeToSign;
	const unsigned int	_gradeToExec;
	bool				_isSigned;

public:
	class GradeTooHighException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Form> Error! Grade too high!");
		}
	};

	class GradeTooLowException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Form> Error! Grade too low!");
		}
	};

	Form(std::string name, unsigned int gradeToSign, unsigned int gradeToExec);
	Form(const Form &f);
	Form	&operator=(const Form &f);
	~Form(void);

	const std::string	&getName(void) const;
	unsigned int		getGradeToSign(void) const;
	unsigned int		getGradeToExec(void) const;
	bool				isSigned(void) const;

	void				beSigned(const Bureaucrat &buro);

};

std::ostream	&operator<<(std::ostream &os, const Form &f);

#endif
