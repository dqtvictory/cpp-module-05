#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat::Bureaucrat(std::string name, unsigned int grade) : _name(name), _grade(grade)
{
	if (grade < GRADE_H)
		throw GradeTooHighException();
	if (grade > GRADE_L)
		throw GradeTooLowException();
}

Bureaucrat::Bureaucrat(const Bureaucrat& b) : _name(b._name), _grade(b._grade)
{}

Bureaucrat	&Bureaucrat::operator=(const Bureaucrat &b)
{
	return ((Bureaucrat&)b);
}

Bureaucrat::~Bureaucrat(void) {}

const std::string	&Bureaucrat::getName(void) const
{
	return (_name);
}

unsigned int	Bureaucrat::getGrade(void) const
{
	return (_grade);
}

void	Bureaucrat::incGrade(void)
{
	if (_grade == GRADE_H)
		throw GradeTooHighException();
	_grade--;
}

void	Bureaucrat::decGrade(void)
{
	if (_grade == GRADE_L)
		throw GradeTooLowException();
	_grade++;
}

void	Bureaucrat::signForm(Form &form)
{
	if (form.isSigned())
	{
		std::cerr << form.getName() << " already signed\n";
		return ;
	}

	try
	{
		form.beSigned(*this);
		std::cout << _name << " signs " << form.getName() << '\n';
	}
	catch (Form::GradeTooLowException &e)
	{
		std::cerr	<< _name << " cannot sign " << form.getName()
					<< " because his grade is too low\n";
	}	
}

void	Bureaucrat::executeForm(Form &form)
{
	try
	{
		form.execute(*this);
		std::cout << _name << " executes " << form.getName() << '\n';
	}
	catch (Form::GradeTooLowException &e)
	{
		std::cerr	<< _name << " cannot execute " << form.getName()
					<< " because his grade is too low\n";
	}
	catch (Form::FormNotSignedException &e)
	{
		std::cerr	<< _name << " cannot execute " << form.getName()
					<< " because this form is not yet signed\n";
	}	
}

std::ostream	&operator<<(std::ostream &os, const Bureaucrat &b)
{
	os << b.getName() << ", bureaucrat grade " << b.getGrade() << '\n';
	return (os);
}
