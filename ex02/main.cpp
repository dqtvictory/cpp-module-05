#include <ctime>
#include <cstdlib>
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int	main(void)
{
	std::srand(time(NULL));

	Bureaucrat	uselessGuy("Useless Guy", 145);
	Bureaucrat	treePlanter("Tree Planter", 100);
	Bureaucrat	roboHandler("Robo Handler", 40);
	Bureaucrat	president("The President", 1);

	Form		*scf = new ShrubberyCreationForm("garden");
	Form		*rrf = new RobotomyRequestForm("42 Paris");
	Form		*ppf = new PresidentialPardonForm("Norminet");

	std::cout << "\nSome useless guy trying to sign a SCF then plant a tree...\n";
	uselessGuy.signForm(*scf);
	uselessGuy.executeForm(*scf);

	std::cout << "\nNo, useless guy must seek help from the tree planter...\n";
	treePlanter.executeForm(*scf);

	std::cout << "\nNow the tree planter overestimates his authority by trying to sign and execute a RRF...\n";
	treePlanter.signForm(*rrf);
	treePlanter.executeForm(*rrf);

	std::cout << "\nBut only the robo handler can do these task\n";
	roboHandler.signForm(*rrf);
	roboHandler.executeForm(*rrf);

	std::cout << "\nIt's time for the president to demonstrate his ultimate power\n";
	president.signForm(*ppf);
	president.executeForm(*ppf);

	delete scf;
	delete rrf;
	delete ppf;
}