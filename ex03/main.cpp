#include <ctime>
#include <cstdlib>
#include "Intern.hpp"

int	main(void)
{
	std::srand(time(NULL));

	Intern	intern;
	Form	*scf = intern.makeForm("ShrubberyCreationForm", "garden");
	Form	*rrf = intern.makeForm("RobotomyRequestForm", "42 Paris");
	Form	*ppf = intern.makeForm("PresidentialPardonForm", "Norminet");
	Form	*invalid = intern.makeForm("Random Form", "nothing");

	Bureaucrat	president("President", 1);

	president.signForm(*scf);
	president.signForm(*rrf);
	president.signForm(*ppf);

	president.executeForm(*scf);
	president.executeForm(*rrf);
	president.executeForm(*ppf);


	delete scf;
	delete rrf;
	delete ppf;
	delete invalid;
}