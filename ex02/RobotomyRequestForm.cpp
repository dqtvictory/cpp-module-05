#include <cstdlib>
#include "RobotomyRequestForm.hpp"

#define FORM_NAME	"RobotomyRequestForm"
#define FORM_SIGN	72
#define FORM_EXEC	45

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form(FORM_NAME, target, FORM_SIGN, FORM_EXEC)
{}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm& f) : Form(FORM_NAME, f.getTarget(), FORM_SIGN, FORM_EXEC)
{}

RobotomyRequestForm	&RobotomyRequestForm::operator=(const RobotomyRequestForm &f)
{
	return ((RobotomyRequestForm&)f);
}

RobotomyRequestForm::~RobotomyRequestForm() {}

void	RobotomyRequestForm::execute(const Bureaucrat &buro) const
{
	checkExecution(buro);

	std::cout << "Rrrrrrrr... Trying to robotomize " << getTarget() << ". ";
	
	int random = std::rand() % 2;
	if (random == 0)
		std::cout << "But FAILED\n";
	else
		std::cout << "SUCCEEDED\n";
}
