#include "Bureaucrat.hpp"

void	displayError(std::exception &e)
{
	std::cerr << "\033[91m" << e.what() << "\033[0m\n";
}

Bureaucrat	*tryCreateBuro(std::string name, unsigned int grade)
{
	std::cout << "Trying to create " << name << " bureaucrat with grade " << grade << "...\n";

	try
	{
		Bureaucrat	*buro = new Bureaucrat(name, grade);
		return (buro);	// If succeed return the new pointer
	}
	catch (Bureaucrat::GradeTooHighException &e)
	{
		std::cerr << "Grade too high exception caught: ";
		displayError(e);
	}
	catch (Bureaucrat::GradeTooLowException &e)
	{
		std::cerr << "Grade too low exception caught: ";
		displayError(e);
	}
	catch (std::exception &e)
	{
		std::cerr << "Unknown exception: ";
		displayError(e);
	}

	return (NULL);		// If fail return NULL
}

int	main(void)
{
	Bureaucrat	*goodBuro = tryCreateBuro("Good buro", 1);
	Bureaucrat	*badBuro = tryCreateBuro("Bad buro", 150);
	Bureaucrat	*okBuro = tryCreateBuro("OK buro", 100);

	std::cout << *goodBuro << *badBuro << *okBuro << '\n';

	Bureaucrat	*tooHigh =	tryCreateBuro("TOO HIGH", 0);
	Bureaucrat	*tooLow =	tryCreateBuro("TOO LOW", 160);

	std::cout << '\n';

	try
	{
		std::cout << "Trying to increment grade of Good buro\n";
		goodBuro->incGrade();

		std::cout << "Trying to decrement grade of Bad buro\n";
		badBuro->decGrade();
	}
	catch (std::exception& e)
	{
		displayError(e);
	}

	std::cout << '\n';

	try
	{
		std::cout << "Trying to increment grade of OK buro\n";
		okBuro->incGrade();
		std::cout << *okBuro;

		std::cout << "Trying to decrement grade of OK buro\n";
		okBuro->decGrade();
		std::cout << *okBuro;
	}
	catch (std::exception& e)
	{
		displayError(e);
	}

	delete goodBuro;
	delete badBuro;
	delete okBuro;
	delete tooHigh;
	delete tooLow;
}