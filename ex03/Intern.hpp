#ifndef INTERN_HPP
#define INTERN_HPP

#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

class Intern
{

typedef	Form* (Intern::*formInit)(const std::string&);

private:
	formInit	initArray[4];
	Form		*makeSCF(const std::string &target);
	Form		*makeRRF(const std::string &target);
	Form		*makePPF(const std::string &target);
	Form		*makeNull(const std::string &target);

public:
	Intern(void);
	Intern(const Intern &i);
	Intern	&operator=(const Intern &i);
	~Intern();

	Form	*makeForm(const std::string &name, const std::string &target);

};


#endif
