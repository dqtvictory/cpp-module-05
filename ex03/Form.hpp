#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"

class Form
{

private:
	const std::string	_name;
	const std::string	_target;
	const unsigned int	_gradeToSign;
	const unsigned int	_gradeToExec;
	bool				_isSigned;

protected:
	void	checkExecution(const Bureaucrat &buro) const;

public:
	class GradeTooHighException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Form> Error! Grade too high!");
		}
	};

	class GradeTooLowException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Form> Error! Grade too low!");
		}
	};

	class FormNotSignedException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Form> Error! Form not signed!");
		}
	};

	Form(std::string name, std::string target, unsigned int gradeToSign, unsigned int gradeToExec);
	Form(const Form &f);
	Form	&operator=(const Form &f);
	virtual ~Form(void) = 0;

	const std::string	&getName(void) const;
	const std::string	&getTarget(void) const;
	unsigned int		getGradeToSign(void) const;
	unsigned int		getGradeToExec(void) const;
	bool				isSigned(void) const;

	void				beSigned(const Bureaucrat &buro);
	virtual void		execute(const Bureaucrat &buro) const = 0;

};

std::ostream	&operator<<(std::ostream &os, const Form &f);

#endif
