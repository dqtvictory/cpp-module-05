#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include <stdexcept>
#include <iostream>

#define GRADE_H	1
#define GRADE_L	150

class Form;

class Bureaucrat
{

private:
	const std::string	_name;
	unsigned int		_grade;

public:
	class GradeTooHighException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Bureaucrat> Error! Grade too high!");
		}
	};

	class GradeTooLowException : public std::exception
	{
	public:
		virtual const char	*what() const throw()
		{
			return ("<Bureaucrat> Error! Grade too low!");
		}
	};

	Bureaucrat(std::string name, unsigned int grade);
	Bureaucrat(const Bureaucrat &b);
	Bureaucrat	&operator=(const Bureaucrat &b);
	~Bureaucrat(void);

	const std::string	&getName(void) const;
	unsigned int		getGrade(void) const;
	void				incGrade(void);
	void				decGrade(void);
	void				signForm(Form &form);

};

std::ostream	&operator<<(std::ostream &os, const Bureaucrat &b);

#endif
