#ifndef SHRUBBERYCREATIONFORM_HPP
#define SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"

class ShrubberyCreationForm : public Form
{

public:
	ShrubberyCreationForm(std::string target);
	ShrubberyCreationForm(const ShrubberyCreationForm &f);
	ShrubberyCreationForm	&operator=(const ShrubberyCreationForm &f);
	~ShrubberyCreationForm(void);

	void	execute(const Bureaucrat &buro) const;

};

#endif
