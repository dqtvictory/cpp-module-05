#include "Form.hpp"

Form::Form(std::string name, std::string target, unsigned int gradeToSign,
unsigned int gradeToExec) :	_name(name), _target(target), _gradeToSign(gradeToSign),
							_gradeToExec(gradeToExec), _isSigned(false)
{
	if (gradeToSign < GRADE_H || gradeToExec < GRADE_H)
		throw GradeTooHighException();
	if (gradeToSign > GRADE_L || gradeToExec > GRADE_L)
		throw GradeTooLowException();
}

Form::Form(const Form& f) :	_name(f._name), _target(f._target), _gradeToSign(f._gradeToSign),
							_gradeToExec(f._gradeToExec), _isSigned(false)
{}

Form	&Form::operator=(const Form &f)
{
	return ((Form&)f);
}

Form::~Form(void) {}

const std::string	&Form::getName(void) const
{
	return (_name);
}

const std::string	&Form::getTarget(void) const
{
	return (_target);
}

unsigned int	Form::getGradeToSign(void) const
{
	return (_gradeToSign);
}

unsigned int	Form::getGradeToExec(void) const
{
	return (_gradeToExec);
}

bool	Form::isSigned(void) const
{
	return (_isSigned);
}

void	Form::beSigned(const Bureaucrat &buro)
{
	if (buro.getGrade() > _gradeToSign)
		throw GradeTooLowException();
	_isSigned = true;
}

void	Form::checkExecution(const Bureaucrat &buro) const
{
	if (_isSigned == false)
		throw FormNotSignedException();
	if (buro.getGrade() > _gradeToExec)
		throw GradeTooLowException();
}

std::ostream	&operator<<(std::ostream &os, const Form &f)
{
	std::string	isSigned = f.isSigned() ? "signed" : "not signed";
	os << f.getName()	<< ", form " << isSigned
						<< ", grade to sign " << f.getGradeToSign()
						<< ", grade to execute " << f.getGradeToExec()
						<< '\n';
	return (os);
}
