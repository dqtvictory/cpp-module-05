#include "PresidentialPardonForm.hpp"

#define FORM_NAME	"PresidentialPardonForm"
#define FORM_SIGN	25
#define FORM_EXEC	5

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form(FORM_NAME, target, FORM_SIGN, FORM_EXEC)
{}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm& f) : Form(FORM_NAME, f.getTarget(), FORM_SIGN, FORM_EXEC)
{}

PresidentialPardonForm	&PresidentialPardonForm::operator=(const PresidentialPardonForm &f)
{
	return ((PresidentialPardonForm&)f);
}

PresidentialPardonForm::~PresidentialPardonForm() {}

void	PresidentialPardonForm::execute(const Bureaucrat &buro) const
{
	checkExecution(buro);

	std::cout << getTarget() << " has been pardoned by Zafod Beeblebrox\n";
}
