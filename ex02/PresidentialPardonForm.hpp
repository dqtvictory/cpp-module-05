#ifndef PRESIDENTIALPARDONFORM_HPP
#define PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

class PresidentialPardonForm : public Form
{

public:
	PresidentialPardonForm(std::string target);
	PresidentialPardonForm(const PresidentialPardonForm &f);
	PresidentialPardonForm	&operator=(const PresidentialPardonForm &f);
	~PresidentialPardonForm(void);

	void	execute(const Bureaucrat &buro) const;

};

#endif
