#include <fstream>
#include "ShrubberyCreationForm.hpp"

#define FORM_NAME	"ShrubberyCreationForm"
#define FORM_SIGN	145
#define FORM_EXEC	137

static std::string	asciiTree(void)
{
	std::string	s = "";
	return (s +	"          .     .  .      +     .      .          .\n"				+
				"     .       .      .     #       .           .\n"					+
				"        .      .         ###            .      .      .\n"			+
				"      .      .   '#:. .:##'##:. .:#'  .      .\n"					+
				"          .      . '####'###'####'  .\n"							+
				"       .     '#:.    .:#'###'#:.    .:#'  .        .       .\n"	+
				"  .             '#########'#########'        .        .\n"			+
				"        .    '#:.  '####'###'####'  .:#'   .       .\n"			+
				"     .     .  '#######''##'##''#######'                  .\n"		+
				"                .'##'#####'#####'##'           .      .\n"			+
				"    .   '#:. ...  .:##'###'###'##:.  ... .:#'     .\n"				+
				"      .     '#######'##'#####'##'#######'      .     .\n"			+
				"    .    .     '#####''#######''#####'    .      .\n"				+
				"            .     '      000      '    .     .\n"					+
				"       .         .   .   000     .        .       .\n"				+
				"........................O000O...................................\n");
}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form(FORM_NAME, target, FORM_SIGN, FORM_EXEC)
{}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm& f) : Form(FORM_NAME, f.getTarget(), FORM_SIGN, FORM_EXEC)
{}

ShrubberyCreationForm	&ShrubberyCreationForm::operator=(const ShrubberyCreationForm &f)
{
	return ((ShrubberyCreationForm&)f);
}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

void	ShrubberyCreationForm::execute(const Bureaucrat &buro) const
{
	checkExecution(buro);

	std::string		fname = getTarget() + "_shrubbery";
	std::ofstream	fs(fname.c_str(), std::ios::trunc);
	
	if (fs.is_open())
	{
		fs << asciiTree();
		fs.close();
	}
	else
		std::cerr << "File cannot be created or opened\n";
}
