#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

class RobotomyRequestForm : public Form
{

public:
	RobotomyRequestForm(std::string target);
	RobotomyRequestForm(const RobotomyRequestForm &f);
	RobotomyRequestForm	&operator=(const RobotomyRequestForm &f);
	~RobotomyRequestForm(void);

	void	execute(const Bureaucrat &buro) const;

};

#endif
