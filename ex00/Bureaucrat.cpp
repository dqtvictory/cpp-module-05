#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(std::string name, unsigned int grade) : _name(name), _grade(grade)
{
	if (grade < GRADE_H)
		throw GradeTooHighException();
	if (grade > GRADE_L)
		throw GradeTooLowException();
}

Bureaucrat::Bureaucrat(const Bureaucrat &b) : _name(b._name), _grade(b._grade)
{}

Bureaucrat	&Bureaucrat::operator=(const Bureaucrat &b)
{
	return ((Bureaucrat&)b);
}

Bureaucrat::~Bureaucrat(void) {}

const std::string	&Bureaucrat::getName(void) const
{
	return (_name);
}

unsigned int	Bureaucrat::getGrade(void) const
{
	return (_grade);
}

void	Bureaucrat::incGrade(void)
{
	if (_grade == GRADE_H)
		throw GradeTooHighException();
	_grade--;
}

void	Bureaucrat::decGrade(void)
{
	if (_grade == GRADE_L)
		throw GradeTooLowException();
	_grade++;
}

std::ostream	&operator<<(std::ostream &os, const Bureaucrat &b)
{
	os << b.getName() << ", bureaucrat grade " << b.getGrade() << '\n';
	return (os);
}
