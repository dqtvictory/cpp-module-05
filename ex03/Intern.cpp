#include "Intern.hpp"

Intern::Intern(void) {}

Intern::Intern(const Intern &i)
{
	*this = i;
}

Intern	&Intern::operator=(const Intern &i)
{
	return ((Intern &)i);
}

Intern::~Intern() {}

Form	*Intern::makeSCF(const std::string &target)
{
	return (new ShrubberyCreationForm(target));
}

Form	*Intern::makeRRF(const std::string &target)
{
	return (new RobotomyRequestForm(target));
}

Form	*Intern::makePPF(const std::string &target)
{
	return (new PresidentialPardonForm(target));
}

Form	*Intern::makeNull(const std::string &target)
{
	(void)target;
	return (NULL);
}

Form	*Intern::makeForm(const std::string &name, const std::string &target)
{
	static bool init = false;

	if (!init)
	{
		initArray[0] = &Intern::makeSCF;
		initArray[1] = &Intern::makeRRF;
		initArray[2] = &Intern::makePPF;
		initArray[3] = &Intern::makeNull;
		init = true;
	}

	const std::string	validNames[] = {
		"ShrubberyCreationForm",
		"RobotomyRequestForm",
		"PresidentialPardonForm"
	};

	Form	*form;
	int	i = 0;
	while (i < 3 && name != validNames[i])
		i++;
	form = (*this.*initArray[i])(target);

	std::string	msg = (form == NULL)
		? ("Intern cannot create " + name)
		: ("Intern creates " + form->getName());

	std::cout << msg << '\n';
	return (form);
}
